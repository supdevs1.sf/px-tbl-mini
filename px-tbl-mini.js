import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import { WiredLink } from "wired-link"

/**
 * `px-tbl-mini`
 * Mini Price Table
 *
 * Author: 
 *  Original Fr. https://codepen.io/joejoinerr/pen/CJAkK
 *  chungyan5@gmail.com
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class PxTblMini extends PolymerElement {

  static get template() {
    return html`
      <style>
        :host {
          display: block;
          background: #454c4f;
          font: 1em/1.5 'Droid Sans', sans-serif;
          padding: 2px 2px 2px 2px;
        }

        *,
        *:before,
        *:after {
          box-sizing: border-box;
        }
        
        .pricing {
          @include translate(-49%, -49.1%);
          position: static;
          left: 50%;
          top: 50%;
          /*width: 310px;*/
          font-size: 0;
          opacity: 1;
          transition: opacity .2s ease;
        }
        
        .price-option {
          display: inline-block;
          width: 6.25em;
          vertical-align: middle;
          margin-right: 0.3125em;
          font-size: 16px;
          opacity: 1;
          transition: opacity .2s ease;
        }
        
        .price-option:last-child { margin-right: 0; }
        
        .pricing:hover .price-option { opacity: 0.6; }
        .pricing:hover .price-option:hover { opacity: 1; }
        
        .price-option__detail {
          padding: 2em 0;
          background: white;
          text-align: center;
        }
        
        .price-option--left .price-option__detail  { border-radius: .25em 0 0 0; }
        .price-option--middle .price-option__detail  { padding: 2.3em 0; border-radius: .25em .25em 0 0; }
        .price-option--right .price-option__detail { border-radius: 0 .25em 0 0; }
        
        .price-option__cost,
        .price-option__type {
          display: block;
        }
        
        .price-option__cost {
          font-size: 1.5em;
          color: #383838;
        }
        
        .price-option__type {
          font-size: .7em;
          text-transform: uppercase;
          color: #909090;
        }
        
        .price-option__purchase {
          position: relative;
          display: block;
          padding: .6em;
          font-size: .875em;
          font-weight: bold;
          text-align: center;
          text-transform: uppercase;
          text-decoration: none;
          color: rgba(0,0,0,0.6);
        }
        
        .price-option--left .price-option__purchase  { background: #eada42; border-radius: 0 0 0 .25em; }
        .price-option--middle .price-option__purchase  { background: #a7d155; border-radius: 0 0 .25em .25em; }
        .price-option--right .price-option__purchase { background: #e54e4b; border-radius: 0 0 .25em 0; }
        
        .price-option__purchase:before {
          content: '';
          position: absolute;
          bottom: 100%;
          left: 50%;
          display: block;
          width: 0;
          height: 0;
          margin-left: -5px;
          border-width: 0 5px 5px;
          border-style: solid;
        }
        
        .price-option--left .price-option__purchase:before  { border-color: transparent transparent #eada42; }
        .price-option--middle .price-option__purchase:before  { border-color: transparent transparent #a7d155; }
        .price-option--right .price-option__purchase:before { border-color: transparent transparent #e54e4b; }        

      </style>

      <div class="pricing">
        <div class="[ price-option price-option--left ]">
          <div class="price-option__detail">
            <span class="price-option__cost">[[hea_txt_l]]</span>

            <span class="price-option__type">
              <!-- if existing of URL, display link  -->
              <template is="dom-if" if="[[desc_url_l]]">
                <wired-link elevation="3" href="[[desc_url_l]]" target="_blank">[[desc_txt_l]]</wired-link>
              </template>
              <template is="dom-if" if="[[!desc_url_l]]">
                [[desc_txt_l]]
              </template>
            </span>

          </div>
          <a href="[[url_l]]" class="price-option__purchase" target="{{url_tgt_l}}" title="[[toast_l]]">
            [[but_txt_l]]
          </a>
        </div>
        <div class="[ price-option price-option--middle ]">
          <div class="price-option__detail">
            <span class="price-option__cost">[[hea_txt_m]]</span>

            <span class="price-option__type">
              <template is="dom-if" if="[[desc_url_m]]">
                <wired-link elevation="3" href="[[desc_url_m]]" target="_blank">[[desc_txt_m]]</wired-link>
              </template>
              <template is="dom-if" if="[[!desc_url_m]]">
                [[desc_txt_m]]
              </template>
            </span>

          </div>        
          <a id="link_m" href="[[url_m]]" class="price-option__purchase" target="{{url_tgt_m}}" title="[[toast_m]]">
            [[but_txt_m]]
          </a>
        </div>
        <div class="[ price-option price-option--right ]">
          <div class="price-option__detail">
            <span class="price-option__cost">[[hea_txt_r]]</span>

            <span class="price-option__type">
              <template is="dom-if" if="[[desc_url_r]]">
                <wired-link elevation="3" href="[[desc_url_r]]" target="_blank">[[desc_txt_r]]</wired-link>
              </template>
              <template is="dom-if" if="[[!desc_url_r]]">
                [[desc_txt_r]]
              </template>
            </span>

          </div>
          <a id="link_r" href="[[url_r]]" class="price-option__purchase" target="{{url_tgt_r}}" title="[[toast_r]]">
            [[but_txt_r]]
          </a>
        </div>
      </div>
    `;
  }
  
  static get properties() {
    return {

      // header texts
      hea_txt_l: {
        type: String,
        value: 'left',
      },
      hea_txt_m: {
        type: String,
        value: 'middle',
      },
      hea_txt_r: {
        type: String,
        value: 'right',
      },

      // description texts
      desc_txt_l: {
        type: String,
        value: 'left',
      },
      desc_txt_m: {
        type: String,
        value: 'middle',
      },
      desc_txt_r: {
        type: String,
        value: 'right',
      },

      // description url
      desc_url_l: {
        type: String,
        value: '',
      },
      desc_url_m: {
        type: String,
        value: '',
      },
      desc_url_r: {
        type: String,
        value: '',
      },

      // toast texts
      toast_l: {
        type: String,
        value: 'left',
      },
      toast_m: {
        type: String,
        value: 'middle',
      },
      toast_r: {
        type: String,
        value: 'right',
      },

      // buttom texts
      but_txt_l: {
        type: String,
        value: 'left',
      },
      but_txt_m: {
        type: String,
        value: 'middle',
      },
      but_txt_r: {
        type: String,
        value: 'right',
      },

      // URL
      url_l: {
        type: String,
        value: '#',
      },
      url_m: {
        type: String,
        value: '#',
      },
      url_r: {
        type: String,
        value: '#',
      },

      // URL Taget
      url_tgt_l: {
        type: String,
        value: '_self',
      },
      url_tgt_m: {
        type: String,
        value: '_self',
      },
      url_tgt_r: {
        type: String,
        value: '_self',
      },

    };
  }

}

window.customElements.define('px-tbl-mini', PxTblMini);
